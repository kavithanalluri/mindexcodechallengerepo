package com.mindex.challenge.data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

public class Compensation {
	private Employee employee;
	
	// Using BigDecimal for maintaining correct precision. Can use String if
	// the currencyType also needs to be saved along with salary
	private BigDecimal salary;
	
	//Using LocalDateTime to store date,time and timezone
	//Sample format: 1970-01-01T00:00:00
	//Please use this format while creating Compensation object
	private LocalDateTime effectiveDate;
	
	public Compensation() {
	}

	public Compensation(Employee employee, BigDecimal salary, LocalDateTime effectiveDate) {
		this.employee = employee;
		this.salary = salary;
		this.effectiveDate = effectiveDate;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public LocalDateTime getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(LocalDateTime effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Compensation other = (Compensation) obj;
		return Objects.equals(effectiveDate, other.effectiveDate) && Objects.equals(salary, other.salary)
				&& Objects.nonNull(employee) ? employee.equals(other.employee)
						: Objects.equals(employee, other.employee);
	}

	@Override
	public int hashCode() {
		return Objects.hash(effectiveDate, employee, salary);
	}

	@Override
	public String toString() {
		return "Compensation [employee=" + employee + ", salary=" + salary + ", effectiveDate=" + effectiveDate + "]";
	}
}
