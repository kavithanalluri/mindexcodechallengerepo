package com.mindex.challenge.data;

import java.util.Objects;

public class ReportingStructure extends Object {
	private Employee employee;
	private int numberOfReports;

	public ReportingStructure() {
	}

	public ReportingStructure(Employee employee, int numberOfReports) {
		this.employee = employee;
		this.numberOfReports = numberOfReports;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getNumberOfReports() {
		return numberOfReports;
	}

	public void getNumberOfReports(int numberOfReports) {
		this.numberOfReports = numberOfReports;
	}

	@Override
	public int hashCode() {
		return Objects.hash(employee, numberOfReports);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		ReportingStructure other = (ReportingStructure) obj;
		return (this.getNumberOfReports() == other.getNumberOfReports() && Objects.nonNull(this.employee)
				? this.getEmployee().equals(other.getEmployee())
				: Objects.equals(this.getEmployee(), other.getEmployee()));
	}

	@Override
	public String toString() {
		return "ReportingStructure [employee=" + employee + ", numberOfReports=" + numberOfReports + "]";
	}

}