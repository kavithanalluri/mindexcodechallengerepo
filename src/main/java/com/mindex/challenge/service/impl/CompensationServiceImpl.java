
package com.mindex.challenge.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.mindex.challenge.dao.CompensationRepository;
import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.service.CompensationService;
import com.mindex.challenge.service.EmployeeService;

@Service
public class CompensationServiceImpl implements CompensationService {
	private static final Logger LOG = LoggerFactory.getLogger(CompensationServiceImpl.class);
	private CompensationRepository compensationRepo;
	private EmployeeService employeeService;

	@Autowired
	public CompensationServiceImpl(CompensationRepository compensationRepo, EmployeeService employeeService) {
		this.compensationRepo = compensationRepo;
		this.employeeService = employeeService;
	}

	/**
	 * Checks if the given compensation has valid empoyeeId and its not a duplicate
	 * record. If it is valid then, creates given compensation object
	 * 
	 * 
	 * NOTE: Only one unique compensation object should exist for any employee.
	 * Otherwise database fails to have integrity. If the compensation needs to
	 * updated, a new(PUT) endpoint has to be created .
	 * 
	 * @param compensation {@link Compensation} that needs to be created
	 * @return created {@link Compensation} object
	 * 
	 * @throws ResponseStatusException throws CONFLICT status in case of a duplicate
	 *                                 record for same employeeId
	 * @throws ResponseStatusException throws BAD_REQUEST status in case
	 *                                 compensation object is incomplete
	 * 
	 */
	@Override
	public Compensation create(Compensation compensation) {
		LOG.debug("Creating compensation [{}]", compensation);

		if (existsNonNullEmployeeId(compensation)) {

			Employee existingEmployee = employeeService.read(compensation.getEmployee().getEmployeeId());
			Compensation existingCompensation = compensationRepo.findByEmployee(existingEmployee);

			if (existingCompensation != null) {
				// TO DO: Create Put operation for updating compensation object
				String errorMessage = "Duplicate Compensation exist for given employeeId: "
						+ compensation.getEmployee().getEmployeeId();
				LOG.error(errorMessage);
				throw new ResponseStatusException(HttpStatus.CONFLICT, errorMessage);
			}

			compensation.setEmployee(existingEmployee);
			compensationRepo.insert(compensation);

		} else {
			LOG.error("Compensation object should have non null employeeId");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"Compensation object should have non null employeeId");
		}

		return compensation;
	}

	/**
	 * Reads Compensation object for given employeeId
	 * 
	 * @param employeeId unique UUID of an employee
	 * @return {@link Compensation} with all compensation details
	 * 
	 * @throws ResponseStatusException thrown if the compensation doesn't exist for
	 *                                 given employeeId
	 */
	@Override
	public Compensation read(String employeeId) {
		LOG.debug("Reading compensation for employeeId [{}]", employeeId);

		Employee employee = employeeService.read(employeeId);

		Compensation compensation = compensationRepo.findByEmployee(employee);
		if (compensation == null) {
			LOG.error("Compensation record not found for given employeeId: " + employeeId);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"Compensation record not found for given employeeId: " + employeeId);
		}

		return compensation;
	}

	private boolean existsNonNullEmployeeId(Compensation compensation) {
		return compensation != null && compensation.getEmployee() != null
				&& compensation.getEmployee().getEmployeeId() != null
				&& !compensation.getEmployee().getEmployeeId().trim().isEmpty();
	}

}
