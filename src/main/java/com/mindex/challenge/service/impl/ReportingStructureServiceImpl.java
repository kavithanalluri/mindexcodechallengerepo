package com.mindex.challenge.service.impl;

import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.EmployeeService;
import com.mindex.challenge.service.ReportingStructureService;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportingStructureServiceImpl implements ReportingStructureService {

	private static final Logger LOG = LoggerFactory.getLogger(ReportingStructureServiceImpl.class);

	private EmployeeService employeeService;

	@Autowired
	public ReportingStructureServiceImpl(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@Override
	public ReportingStructure read(String id) {
		LOG.debug("Reading ReportingStructure for id [{}]", id);

		Employee employee = employeeService.read(id);
		
		// We are updating the employee object also with fully filled out details by sending
		// it as passByReference.
		int totalNumberOfReports = getTotalNumberOfDistinctReports(employee);

		ReportingStructure reportingStructure = new ReportingStructure(employee, totalNumberOfReports);
		return reportingStructure;
	}

	/**
	 * Gets total number of distinct employees reporting(directly or indirectly) to
	 * given employee
	 * 
	 * NOTE: We run into an issue in case the reporting structure has circular
	 * dependencies. Constraints needs to be added to make no circular dependencies are
	 * allowed.
	 * 
	 * @param employee {@link Employee} contains employee details and their
	 *                 directReports employeeIds
	 * 
	 * @return total number of employees reporting to given employee
	 */
	private int getTotalNumberOfDistinctReports(Employee employee) {
		// Set removes duplicates so, using Set to get all distinct reporting employees
		// in case any employee is reporting to more than one boss
		Set<String> reportingEmployeeSet = new HashSet<>();

		// Using Queue to read the number of directReports for given employee and all of
		// their direct reports
		Queue<Employee> reportingEmployeeQ = new LinkedList<>();
		reportingEmployeeQ.addAll(employee.getDirectReports());

		while (reportingEmployeeQ.size() > 0) {
			Employee reportingEmployee = reportingEmployeeQ.remove();
			String employeeId = reportingEmployee.getEmployeeId();

			// Adding each reporting employeeId to set
			reportingEmployeeSet.add(employeeId);

			Employee reportingEmployeeWithDirectReports = employeeService.read(employeeId);
			populateEmployeeDetails(reportingEmployee, reportingEmployeeWithDirectReports);
			
			reportingEmployeeQ.addAll(reportingEmployeeWithDirectReports.getDirectReports());
		}

		return reportingEmployeeSet.size();
	}

	/**
	 * The main employee in the reporting structure only contains their
	 * directReports employmentIds but not their details. Using this method to populate each employee with its full
	 * details and their directReports.
	 * 
	 * @param employee                  {@link Employee} employee with just
	 *                                  employeeId
	 * @param employeeWithDirectReports {@link Employee} contains employee details
	 *                                  and their directReports employeeIds
	 */
	private void populateEmployeeDetails(Employee employee, Employee employeeWithDirectReports) {
		employee.setFirstName(employeeWithDirectReports.getFirstName());
		employee.setLastName(employeeWithDirectReports.getLastName());
		employee.setPosition(employeeWithDirectReports.getPosition());
		employee.setDepartment(employeeWithDirectReports.getDepartment());
		employee.setDirectReports(employeeWithDirectReports.getDirectReports());
	}
}
