package com.mindex.challenge.service.impl;

import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.Compensation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CompensationServiceImplTest {

	private String createCompensationUrl;
	private String readCompensationUrl;

	@Autowired
	private EmployeeRepository employeeRepository;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	private Employee employee;

	@Before
	public void setup() {
		createCompensationUrl = "http://localhost:" + port + "/compensation";
		readCompensationUrl = "http://localhost:" + port + "/compensation/employee/{id}";

		employee = employeeRepository.findByEmployeeId("16a596ae-edd3-4847-99fe-c4518e82c86f");
	}

	@Test
	public void testCreateReadUpdate() {
		Compensation compensation = new Compensation(employee, new BigDecimal(110000.56), LocalDateTime.now());

		// Create checks
		ResponseEntity<Compensation> createdCompensationResponse = restTemplate.postForEntity(createCompensationUrl,
				compensation, Compensation.class);
		assertEquals(HttpStatus.OK, createdCompensationResponse.getStatusCode());
		Compensation createdCompensation = (Compensation) createdCompensationResponse.getBody();
		assertNotNull(createdCompensation);
		assertEquals(compensation, createdCompensation);

		Compensation duplicateCompensationForSameEmployee = new Compensation(employee, new BigDecimal(123000.56),
				LocalDateTime.now());

		// Create checks
		ResponseEntity<Compensation> duplicateCompensationResponse = restTemplate.postForEntity(createCompensationUrl,
				duplicateCompensationForSameEmployee, Compensation.class);
		assertEquals(HttpStatus.CONFLICT, duplicateCompensationResponse.getStatusCode());

		// Read checks
		ResponseEntity<Compensation> readCompensationResponse = restTemplate.getForEntity(readCompensationUrl,
				Compensation.class, createdCompensation.getEmployee().getEmployeeId());
		assertEquals(HttpStatus.OK, readCompensationResponse.getStatusCode());
		Compensation readCompensation = (Compensation) readCompensationResponse.getBody();
		assertNotNull(readCompensation);
		createdCompensation.equals(readCompensation);

		// Read checks
		ResponseEntity<Compensation> invalidIdResponse = restTemplate.getForEntity(readCompensationUrl,
				Compensation.class, "Invalid Id");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, invalidIdResponse.getStatusCode());
		
		// Read checks
		ResponseEntity<Compensation> noCompensastionExistsResponse = restTemplate.getForEntity(readCompensationUrl,
				Compensation.class, "c0c2293d-16bd-4603-8e08-638a9d18b22c");
		assertEquals(HttpStatus.NOT_FOUND, noCompensastionExistsResponse.getStatusCode());
	}
}
