package com.mindex.challenge.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReportingStructureServiceImplTest {
	private String reportingStructureIdUrl;
	private String employeeUrl;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Before
	public void setup() {
		employeeUrl = "http://localhost:" + port + "/employee";
		reportingStructureIdUrl = "http://localhost:" + port + "/reportingStructure/employee/{id}";

	}

	@Test
	public void testCreateReadUpdate() {
		Employee employee = new Employee();
		employee.setFirstName("Alice");
		employee.setLastName("Peters");
		employee.setDepartment("Engineering");
		employee.setPosition("Intern");

		// Create checks
		Employee createdEmployee = restTemplate.postForEntity(employeeUrl, employee, Employee.class).getBody();
		assertNotNull(createdEmployee.getEmployeeId());

		Employee dev = new Employee();
		dev.setFirstName("Bob");
		dev.setLastName("Crow");
		dev.setDepartment("Engineering");
		dev.setPosition("Developer II");
		dev.getDirectReports().add(new Employee(createdEmployee.getEmployeeId()));

		// Create checks
		Employee createdDev = restTemplate.postForEntity(employeeUrl, dev, Employee.class)
				.getBody();

		assertNotNull(createdDev.getEmployeeId());
		assertEquals(1, createdDev.getDirectReports().size());

		Employee lead = new Employee();
		lead.setFirstName("Carry");
		lead.setLastName("Griggs");
		lead.setDepartment("Engineering");
		lead.setPosition("Team Lead");
		lead.getDirectReports().add(new Employee(createdEmployee.getEmployeeId()));

		// Create checks
		Employee createdTeamLead = restTemplate.postForEntity(employeeUrl, lead, Employee.class).getBody();

		assertNotNull(createdTeamLead.getEmployeeId());
		assertEquals(1, createdTeamLead.getDirectReports().size());

		Employee manager = new Employee();
		manager.setFirstName("Don");
		manager.setLastName("Ryan");
		manager.setDepartment("Engineering");
		manager.setPosition("Manager");
		List<Employee> employees = new ArrayList<>();
		employees.add(new Employee(createdDev.getEmployeeId()));
		employees.add(new Employee(createdTeamLead.getEmployeeId()));
		
		manager.getDirectReports().addAll(employees);
		
		// Create checks
		Employee createdManager = restTemplate.postForEntity(employeeUrl, manager, Employee.class).getBody();

		assertNotNull(createdTeamLead.getEmployeeId());
		assertEquals(2, createdManager.getDirectReports().size());

		// Read checks
		String id = createdManager.getEmployeeId();
        ResponseEntity<ReportingStructure> readReportingStructureResponse = restTemplate.getForEntity(reportingStructureIdUrl, ReportingStructure.class, id);
        assertEquals(HttpStatus.OK, readReportingStructureResponse.getStatusCode());
        ReportingStructure readReportingStructure = (ReportingStructure)readReportingStructureResponse.getBody();
        assertNotNull(readReportingStructure);
        
        /*
         * If we look at the reportingStructure created above there are only 3 distinct employees reporting to manager.
         * Intern is reporting to two people(Dev & Lead) so, we only counted him once.
         */
        assertEquals(3, readReportingStructure.getNumberOfReports());
        assertEquals(id, readReportingStructure.getEmployee().getEmployeeId());
	}
}
